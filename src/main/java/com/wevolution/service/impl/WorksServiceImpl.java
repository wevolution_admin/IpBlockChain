package com.wevolution.service.impl;

import java.io.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import aws.s3.AWSS3Client;
import aws.s3.util.ConfigHelper;
import cn.tiandechain.jbcc.util.GsonUtil;
import com.alibaba.fastjson.JSON;
import com.wevolution.common.utils.*;
import com.wevolution.domain.*;
import org.fit.cssbox.demo.ImageRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.github.pagehelper.PageHelper;
import com.tiande.jbcc.clien.JBCCClien;
import com.wevolution.ipchain.ConnectionManager;
import com.wevolution.mapper.CopyrightAuditMapper;
import com.wevolution.mapper.WorksInfoMapper;
import com.wevolution.mapper.WorksMapper;
import com.wevolution.mapper.WorksSampleMapper;
import com.wevolution.service.WorksService;

import cn.tiandechain.jbcc.bean.DataBuilder;
import cn.tiandechain.jbcc.bean.QueryResult;
import cn.tiandechain.jbcc.message.JBCCResult;
import cn.tiandechain.jbcc.message.RegulationType;
import cn.tiandechain.jbcc.message.Transaction;
import cn.tiandechain.jbcc.util.BCKeyStoreUtil;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

@Service
public class WorksServiceImpl implements WorksService {
	private static Logger logger = LoggerFactory.getLogger(WorksService.class);
	
	@Resource
	private WorksMapper worksMapper;

	@Resource
	private WorksInfoMapper worksInfoMapper;

	@Resource
	private WorksSampleMapper worksSampleMapper;
	
	@Resource
	private CopyrightAuditMapper copyrightAuditMapper;

	@Value("${jbcc.store.name}")
	private String storeName;
	
	@Value("${jbcc.store.pass}")
	private String storePass;
	
	@Value("${jbcc.alias}")
	private String alias;
	
	@Value("${background.domain}")
	private String domain;

	@Override
	@Transactional("txManager")
	public String registerWorks(Works works) {
		String ret = null;
		try {
			if (works.getWorksId() == null) {// 新增
				works.setWorksId(UUIDTool.getUUID());                               //----------------WorksId
				works.setCreatedTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));//----------------CreatedTime
				worksMapper.insertWorks(works);
			} else {// TODO 更新 待定
				works.setUpdateTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
				worksMapper.updateWorks(works);
			}
			ret = works.getWorksId();
		} catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return ret;
	}

	@Override
	public List<Works> selectWorksByUserId(Integer pageNum, Integer pageSize, Works param) {
		PageHelper.startPage(pageNum, pageSize);
		List<Works> list = worksMapper.selectWorksByUserId(param);
		return list;
	}

	@Override
	public Works selectWorksById(String worksId) {
		Works works = worksMapper.selectWorksById(worksId);
		return works;
	}

	@Override
	@Transactional("txManager")
	public String approvedWorks(Works works,CopyrightAudit audit) {
		int ret = 0;
		try {
			works.setApprovedTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
			int uret = worksMapper.updateWorks(works);
			if(uret<1)
				return "更新失败";
			copyrightAuditMapper.updateByworksId(audit);
		} catch (Exception e) {
			e.printStackTrace();
			ret = -1;
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return String.valueOf(ret);
	}

	@Override
	public List<Works> getWorksList(Integer pageNum, Integer pageSize, Works param) {
		PageHelper.startPage(pageNum, pageSize);
		List<Works> list = worksMapper.selectWorksList(param);
		// PageInfo pageInfo = new PageInfo(list);
		// Page page = (Page) list;
		return list;
	}

	/**
	 * 检查是否有重复交易
	 * 
	 * @param req_no
	 * @return duplicateTran true:重复交易；false:不是重复交易
	 * @throws Exception
	 * @author liwh
	 */
	private boolean checkReqNo(String req_no, JBCCClien jbccClient) throws Exception {
		Map<String, Object> conditionMap = new HashMap<String, Object>();
		conditionMap.put("req_no", req_no);
		/*
		 * duplicateTran取值 true:重复交易 ；false:非重复交易
		 */
		boolean duplicateTran = jbccClient.checkReqNo("tdbc", "works", conditionMap);
		logger.info("[checkReqno " + req_no + "],duplicateTran:" + duplicateTran);
		return duplicateTran;
	}
	@Override
	public QueryResult selectBlock(String id, String hash, String begindate, String endDate, Integer pageNum,
			Integer pageSize) {
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien jbccClient = ConnectionManager.getConnection();
		Map<String, Object> conditionMap = new HashMap<String, Object>();
		if(!StringUtil.isEmpty(id)){
			conditionMap.put("id", id);
		}else{
			if(!StringUtil.isEmpty(hash))
				conditionMap.put("hash", hash);
			else{
				if(!StringUtil.isEmpty(begindate)&&!StringUtil.isEmpty(endDate)){
					if(begindate.equals(endDate)){//开始时间与结束日期相同，结束时间后推一天
						/*Calendar calendar = Calendar.getInstance();
						calendar.setTime(DateUtil.stringToDate(endDate));
						calendar.add(Calendar.DAY_OF_MONTH, 1);
						endDate = DateUtil.dateToString(calendar.getTime(), DateUtil.DATE_FORMAT);*/
						try {
							endDate = DateUtils.formatDate2ShortString(DateUtils.getNextDay(DateUtils.praseString2Date(begindate)));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
				}else{//默认查询当天
					/*begindate = DateUtil.getCurrentDateString(DateUtil.DATE_FORMAT);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(DateUtil.stringToDate(begindate));
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					endDate = DateUtil.dateToString(calendar.getTime(), DateUtil.DATE_FORMAT);*/
					begindate = DateUtils.formatDate2ShortString(new Date());
					endDate = DateUtils.formatDate2ShortString(DateUtils.getNextDay(new Date()));
				}
				conditionMap.put("beginDate", begindate);
				conditionMap.put("endDate", endDate);
				conditionMap.put("endPos", pageSize); // 每次查询最大返回条数 默认10条
				conditionMap.put("startPos", (pageNum - 1) * pageSize);// 偏移量，从0开始，可以是每页的起始数
			}
		}
		QueryResult queryResult = null;
		try {//判断是否范围查询
			if(!StringUtil.isEmpty(id)||!StringUtil.isEmpty(hash)){
				queryResult = jbccClient.selectBlockByCondition("tdbc", conditionMap);
				logger.info("id或hash查询开始");
			}else{
				queryResult = jbccClient.selectBlockListByPage("tdbc", conditionMap);
				logger.info("时间范围查询开始");
				logger.info("begindate:"+begindate+",endDate:"+endDate);
			}
			JBCCResult r = queryResult.getJbccResult();
			logger.info("printJbccResult errorCode:" + r.getErrorCode() + " batchNo:" + r.getBatchNo() + " errorMsg:" + r.getErrorMsg());
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			ConnectionManager.closeConnection();
		}
		return queryResult;
	}
	@Override
	public List<CopyrightAudit> getAudit(Integer pageNum, Integer pageSize, CopyrightAudit param) {
		PageHelper.startPage(pageNum, pageSize);
		List<CopyrightAudit> list = copyrightAuditMapper.selectAuditList(param);
		return list;
	}
	@Override
	public CopyrightAudit getAuditStatus(String worksId) {
		CopyrightAudit copyrightAudit = copyrightAuditMapper.selectByWorksId(worksId).get(0);
		return copyrightAudit;
	}
	@Override
	public Map<String, Object> queryWorksRegister(String worksId) {
		CopyrightAudit copyrightAudit = copyrightAuditMapper.selectByWorksId(worksId).get(0);
		Map<String, Object> map = new HashMap<String, Object>();
		if(copyrightAudit!=null){
			BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
			JBCCClien jbccClient = ConnectionManager.getConnection();
			Map<String, Object> conditionMap = new HashMap<String, Object>();
			conditionMap.put("block_id", copyrightAudit.getBlockId().toString());
			conditionMap.put("works_id", worksId);
	    	
			try {
				QueryResult queryResult = jbccClient.selectBlockWorksInfo("tdbc",conditionMap);
				if (queryResult != null) {
					JBCCResult result = queryResult.getJbccResult();
					logger.info("printJbccResult errorCode:" + result.getErrorCode() + " batchNo:" + result.getBatchNo() + " errorMsg:" + result.getErrorMsg());
					String errorCode = result.getErrorCode();
					if ("0".equals(errorCode)) {// 交易执行成功
						List<Map<String, Object>> resultlist = queryResult.getResultlist();
						if (!resultlist.isEmpty()) {
							map = resultlist.get(0);
							Timestamp timestamp = DateUtil.stringDatetimeToTimestampss(map.get("timestamp").toString());
							map.put("timestamp", timestamp);
							String works_certificate_url = "";
							String url = null;
							try {
								HashMap<String, String> paramMap = new HashMap<>();
								paramMap.put("works_id", worksId);
								url = HttpInvoker.getInstance().invoke(domain+"download.do", paramMap, false);
							} catch (Exception e) {
								e.printStackTrace();
							}
							@SuppressWarnings("unchecked")
							Map<String,String> jsonToMap = JacksonUtil.jsonToMap(url);
							if(jsonToMap != null&&jsonToMap.containsKey("url")){
								works_certificate_url = jsonToMap.get("url");
							}
							map.put("works_certificate_url", works_certificate_url);
						} 
					}else{
						throw new RuntimeException("连接区块链异常");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("查询失败");
			}finally {
				ConnectionManager.closeConnection();
			}
		}
		return map;
	}
	@Override
	public List<Map<String, Object>> reSendMsg(String[] worksIds) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Map<String, String>> getWorksAndInfo(Integer pageNum, Integer pageSize,String type,Integer area) {
		PageHelper.startPage(pageNum, pageSize);
		List<Map<String, String>> list = worksMapper.selectWorksAndInfo(type,area);
		return list;
	}
	@Override
	public Map<String, String> getWorksAndInfoByWorksId(String worksId) {
		Map<String, String> map = worksMapper.selectWorksAndInfoByWorksId(worksId);
		return map;
	}

	@Override
	@Transactional("txManager")
	public JBCCResult sendMsg(String worksId) {
//		BCKeyStoreUtil.createKeyStore("admin_1", "123", "block_chain_user");
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien connection = ConnectionManager.getConnection();
		String outerSerial = UUID.randomUUID().toString().substring(0, 32);
		String reqNo = outerSerial.substring(outerSerial.length() - 5, outerSerial.length() - 1);
		//audit_information initializing
		List<WorksInfo> infoList = worksInfoMapper.selectWorksInfoByWorksId(worksId);
		String date = DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H);
		StringBuffer authors = new StringBuffer();
		StringBuffer authorSings = new StringBuffer();
		for (WorksInfo worksInfo : infoList) {
			authors.append(worksInfo.getAuthor()+"|");
			authorSings.append(worksInfo.getAuthorSing()+"|");
		}
		CopyrightAudit audit = new CopyrightAudit();
		WorksInfo info = infoList.get(0);
		audit.setReqNo(reqNo);
		audit.setWorksId(info.getWorksId());
		audit.setWorksName(info.getWorksName());
		audit.setUserId(info.getUserId());
		audit.setAuthor(StringUtil.removeLastChar(authors.toString(), "|").toString());
		audit.setAuthorSing(StringUtil.removeLastChar(authorSings.toString(), "|").toString());
		audit.setApprovedStatus((byte) 1);//（1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败）
		audit.setCreatedTime(date);
		audit.setUpdateFlag("0");
		copyrightAuditMapper.insert(audit);

		boolean checkReqNo = false;
		try {
			checkReqNo = checkReqNo(reqNo, connection);
			logger.info("repeating transaction:" + checkReqNo);
		} catch (Exception e1) {
			e1.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		JBCCResult r = null;
		if (checkReqNo) {
			return r;
		}
		List<DataBuilder> dataList = new ArrayList<DataBuilder>();
		// works
		DataBuilder works = new DataBuilder("tdbc", "works", "n", outerSerial);


		Map<String, String> worksMap = worksMapper.queryWorksById(worksId);
		worksMap.put("req_no", reqNo);
		works.setParamMap(worksMap);

		Transaction tran = new Transaction();
		tran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
		works.setTransaction(tran);
		dataList.add(works);

		// works_info
		List<Map<String, String>> list = worksInfoMapper.queryWorksInfoByWorksId(worksId);
		list.forEach(map -> {
			DataBuilder worksInfo = new DataBuilder("tdbc", "works_info", "n", outerSerial);
			worksInfo.setParamMap(map);
			Transaction worksInfoTran = new Transaction();
			worksInfoTran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
			worksInfo.setTransaction(worksInfoTran);
			dataList.add(worksInfo);
		});
		// works_sample
		Map<String, String> sampleMap = worksSampleMapper.querySampleByWorksId(worksId);
		DataBuilder worksSample = new DataBuilder("tdbc", "works_sample", "n", outerSerial);
		worksSample.setParamMap(sampleMap);
		Transaction worksSampleTran = new Transaction();
		worksSampleTran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
		worksSample.setTransaction(worksSampleTran);
		dataList.add(worksSample);

		// send works information
		CopyrightAudit copyrightAudit = new CopyrightAudit();
		copyrightAudit.setWorksId(worksId);

		Works worksData = new Works();
		worksData.setWorksId(worksId);
		try {
			r = connection.sendAndCountReturnForIPBlockChain(dataList);
			logger.info("The return code of sending works information:" + r.getErrorCode() + " batchNo:" + r.getBatchNo() + " errorMsg:" + r.getErrorMsg()+"blockId:"+r.getBlockId());
			if("0".equals(r.getErrorCode())){
				if(StringUtil.isEmpty(r.getBlockId())){
					copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					throw new RuntimeException("返回blockId异常");
				}else{
					copyrightAudit.setApprovedStatus((byte) 2);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					copyrightAudit.setBlockId(Integer.parseInt(r.getBlockId()));

					worksData.setApprovedStatus((byte) 2);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				}
			}else if("TDBC3009".equals(r.getErrorCode())){
				copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
			}else{
				copyrightAudit.setApprovedStatus((byte) 3);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				worksData.setApprovedStatus((byte) 3);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
			}
			copyrightAudit.setUpdateTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
			worksData.setUpdateTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
		} catch (Exception e) {
			e.printStackTrace();
			copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
			worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
		}finally {
			copyrightAuditMapper.updateByworksId(copyrightAudit);
			worksMapper.updateWorks(worksData);
			ConnectionManager.closeConnection();
		}
		return r;
	}

	@Override
	@Transactional("txManager")
	public String sendMsgApi(List<WorksInfo> infoLists, Works workLists, WorksSample worksSamples) {
//		BCKeyStoreUtil.createKeyStore("admin_1", "123", "block_chain_user");
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien connection = ConnectionManager.getConnection();
		String outerSerial = UUID.randomUUID().toString().substring(0, 32);
		String reqNo = outerSerial.substring(outerSerial.length() - 5, outerSerial.length() - 1);
		//audit_information initializing
		List<WorksInfo> infoList = infoLists;
		String date = DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H);
		StringBuffer authors = new StringBuffer();
		StringBuffer authorSings = new StringBuffer();
		for (WorksInfo worksInfo : infoList) {
			authors.append(worksInfo.getAuthor()+"|");
			authorSings.append(worksInfo.getAuthorSing()+"|");
		}
		CopyrightAudit audit = new CopyrightAudit();
		WorksInfo info = infoList.get(0);
		audit.setReqNo(reqNo);
		audit.setWorksId(info.getWorksId());
		audit.setWorksName(info.getWorksName());
		audit.setUserId(info.getUserId());
		audit.setAuthor(info.getAuthor());
		audit.setAuthorSing(info.getAuthorSing());
		audit.setApprovedStatus((byte) 1);//（1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败）
		audit.setCreatedTime(date);
		audit.setUpdateFlag("0");
		copyrightAuditMapper.insert(audit);

		boolean checkReqNo = false;
		try {
			checkReqNo = checkReqNo(reqNo, connection);
			logger.info("repeating transaction:" + checkReqNo);
		} catch (Exception e1) {
			e1.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		JBCCResult r = null;
		if (checkReqNo) {
//			return r.getErrorCode();
			return ResponseUtil.getResponseJson(-1, "3",r);
		}
		List<DataBuilder> dataList = new ArrayList<DataBuilder>();
		// works
		DataBuilder dataBuilderworks = new DataBuilder("tdbc", "works", "n", outerSerial);
		worksMapper.insertWorks(workLists);
		Map<String, String> worksMap = worksMapper.queryWorksById(workLists.getWorksId());
		worksMap.put("req_no", reqNo);
		/*worksMap.put("user_id", "8eef7dc5f4634acebc931bc500f6496d");
		worksMap.put("works_id", "bf518b39b18248c8afbe801c7e4657a1");
		worksMap.put("works_name", "智慧生物3");
		worksMap.put("works_sign", "小天");
		worksMap.put("works_url", "1234");
		worksMap.put("created_time", "2018-02-01 08:14:41");
		worksMap.put("update_time", "2018-02-01 08:14:41");
		worksMap.put("approved_time", "2018-02-01 08:14:41");
		worksMap.put("approved_description", "123");
		worksMap.put("approved_status", "2");*/
		dataBuilderworks.setParamMap(worksMap);
		Transaction tran = new Transaction();
		tran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
		dataBuilderworks.setTransaction(tran);
		dataList.add(dataBuilderworks);

//		for(int x = 0;x<1;x++){
//			mapList.put("updated_time", "2018-05-28 16:06:37");
//			mapList.put("author_sing", "张小良");
//			mapList.put("city", "上海市");
//			mapList.put("works_explain", "");
//			mapList.put("owner_city", "无锡");
//			mapList.put("works_nature_explain", "123");
//			mapList.put("access_rights_type", "原始");
//			mapList.put("works_nature", "原创");
//			mapList.put("province", "上海市");
//			mapList.put("works_id", "bf518b39b18248c8afbe801c7e4657a1");
//			mapList.put("publish_status", "2");
//			mapList.put("area", "黄浦区");
//			mapList.put("works_name", "智慧生物3");
//			mapList.put("created_time", "2018-05-28 16:06:37");
//			mapList.put("accomplish_time", "2018-05-28 16:06:37");
//			mapList.put("owner_name", "张洪量");
//			mapList.put("owner_province", "江苏");
//			mapList.put("author", "张洪亮");
//			mapList.put("owner_id_number", "321023199202121013");
//			mapList.put("works_type", "文字");
//			mapList.put("rights_affiliation_type", "个人作品");
//			mapList.put("own_right_status_explain", "123");
//			mapList.put("publish_address", "123");
//			mapList.put("electronic_medium","2");
//			mapList.put("user_id", "8eef7dc5f4634acebc931bc500f6496d");
//			mapList.put("first_publish_status", "");
//			mapList.put("rights_affiliation_type_explain", "");
//			mapList.put("access_rights_type_explain", "");
//			mapList.put("owner_nationality", "中国");
//			mapList.put("own_right_status", "发表权,署名权");
//			mapList.put("owner_id_type", "居民身份证");
//			mapList.put("owner_sign", "正常");
//			mapList.put("owner_type", "自然人");
//		}
		// works_info
		worksInfoMapper.insertWorksInfoByOne(infoLists.get(0));
		List<Map<String, String>> list = worksInfoMapper.queryWorksInfoByWorksId(infoLists.get(0).getWorksId());
		list.forEach(map -> {
			DataBuilder worksInfo = new DataBuilder("tdbc", "works_info", "n", outerSerial);
			worksInfo.setParamMap(map);
			Transaction worksInfoTran = new Transaction();
			worksInfoTran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
			worksInfo.setTransaction(worksInfoTran);
			dataList.add(worksInfo);
		});

		// works_sample
		worksSampleMapper.insertWorksSample(worksSamples);
		Map<String, String> sampleMap = worksSampleMapper.querySampleByWorksId(worksSamples.getWorksId());
//		Map<String, String> sampleMap = new HashMap<>();
//		WorksSample sample = worksSamples;
//		WorksSample sample = new WorksSample();
//		sampleMap.put("created_time", "2018-05-28 16:06:37");
//		sampleMap.put("works_url", "123");
//		sampleMap.put("update_time", "2018-05-28 16:06:37");
//		sampleMap.put("works_name", "智慧生物3");
//		sampleMap.put("user_id", "8eef7dc5f4634acebc931bc500f6496d");
//		sampleMap.put("works_id", "bf518b39b18248c8afbe801c7e4657a1");
//		sampleMap.put("approved_time", "2018-05-28 16:06:37");
		DataBuilder dataBuilderworksSample = new DataBuilder("tdbc", "works_sample", "n", outerSerial);
		dataBuilderworksSample.setParamMap(sampleMap);
		Transaction worksSampleTran = new Transaction();
		worksSampleTran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
		dataBuilderworksSample.setTransaction(worksSampleTran);
		dataList.add(dataBuilderworksSample);

		CopyrightAudit copyrightAudit = new CopyrightAudit();
		copyrightAudit.setWorksId(worksSamples.getWorksId());
//
		Works worksData = new Works();
		worksData.setWorksId(worksSamples.getWorksId());
		int status = 4;
		try {
			r = connection.sendAndCountReturnForIPBlockChain(dataList);
			logger.info("The return code of sending works information:" + r.getErrorCode() + " batchNo:" + r.getBatchNo() + " errorMsg:" + r.getErrorMsg()+"blockId:"+r.getBlockId());
			if("0".equals(r.getErrorCode())){
				if(StringUtil.isEmpty(r.getBlockId())){
					copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					status = 4;
					throw new RuntimeException("返回blockId异常");
				}else{
					copyrightAudit.setApprovedStatus((byte) 2);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					copyrightAudit.setBlockId(Integer.parseInt(r.getBlockId()));
					worksData.setApprovedStatus((byte) 2);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
					status = 2;
				}
			}else if("TDBC3009".equals(r.getErrorCode())){
				copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				status = 4;
			}else{
				copyrightAudit.setApprovedStatus((byte) 3);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				worksData.setApprovedStatus((byte) 3);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
				status = 3;
			}
			copyrightAudit.setUpdateTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
			worksData.setUpdateTime(DateUtil.getCurrentDateString(DateUtil.DATETIMEPATTERN24H));
		} catch (Exception e) {
			e.printStackTrace();
			status = 4;
			copyrightAudit.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
			worksData.setApprovedStatus((byte) 4);//1：录入；2：共识成功；3：共识失败；4：共识超时；5：审核成功；6审核失败
		}finally {
			copyrightAuditMapper.updateByworksId(copyrightAudit);
			worksMapper.updateWorks(worksData);
			ConnectionManager.closeConnection();
		}
		return ResponseUtil.getResponseJson(0, status+"",r);
	}

	/**
	 * WorkOpus workLists
	 * @return
	 */
	@Override
	public String sendOpusApi(WorkOpus works) {
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien connection = ConnectionManager.getConnection();
		String outerSerial = UUID.randomUUID().toString().substring(0, 32);
		String reqNo = outerSerial.substring(outerSerial.length() - 5, outerSerial.length() - 1);
		boolean checkReqNo = false;
		try {
			checkReqNo = checkReqNo(reqNo, connection);
			logger.info("repeating transaction:" + checkReqNo);
		} catch (Exception e1) {
			e1.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		JBCCResult r = null;
		if (checkReqNo) {
//			return r.getErrorCode();
			return ResponseUtil.getResponseJson(-1, "3",r);
		}
		List<DataBuilder> dataList = new ArrayList<DataBuilder>();
		// works
		DataBuilder dataBuilderworks = new DataBuilder("tdbc", "weopus", "n", outerSerial);

		dataBuilderworks.addParam("req_no", reqNo);// 唯一标识一次请求
		dataBuilderworks.addParam("opus_id", works.getOpusId());
		dataBuilderworks.addParam("userid", works.getUserId());
		dataBuilderworks.addParam("opus_name", works.getOpusName());
		dataBuilderworks.addParam("author", works.getAuthor());
		dataBuilderworks.addParam("opus_category", works.getOpusCategory());
		dataBuilderworks.addParam("opus_type", works.getOpusType());
		dataBuilderworks.addParam("create_time", works.getCreatTime());
		dataBuilderworks.addParam("opus_area", works.getOpusArea());
		dataBuilderworks.addParam("affiliation", works.getAffiliation());
		Transaction tran = new Transaction();
		tran.setRegulationType(RegulationType.REGULATION_ORIGINAL_SAVE);
		dataBuilderworks.setTransaction(tran);
		dataList.add(dataBuilderworks);
		int status = 4;
		try {
			r = connection.sendAndCountReturnForIPBlockChain(dataList);
			logger.info("The return code of sending works information:" + r.getErrorCode() + " batchNo:" + r.getBatchNo() + " errorMsg:" + r.getErrorMsg()+"blockId:"+r.getBlockId());
			if("0".equals(r.getErrorCode())){
				if(StringUtil.isEmpty(r.getBlockId())){
					status = 4;
					throw new RuntimeException("返回blockId异常");
				}else{
					status = 2;
				}
			}else if("TDBC3009".equals(r.getErrorCode())){
				status = 4;
			}else{
				status = 3;
			}
		} catch (Exception e) {
			e.printStackTrace();
			status = 4;
		}finally {
			ConnectionManager.closeConnection();
		}
		return ResponseUtil.getResponseJson(0, status+"",r);
	}

	@Override
	public Map<String, Object> getsampleQueryInfoApi(String worksId,String blockId) {
		Map<String, Object> map = new HashMap<String, Object>();
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien jbccClient = ConnectionManager.getConnection();
		Map<String, Object> conditionMap = new HashMap<String, Object>();
		conditionMap.put("block_id", blockId);
		conditionMap.put("works_id", worksId);
		try {
			QueryResult queryResult = jbccClient.selectBlockWorksInfo("tdbc",conditionMap);
			if (queryResult != null) {
				JBCCResult result = queryResult.getJbccResult();
				logger.info("printJbccResult errorCode:" + result.getErrorCode() + " batchNo:" + result.getBatchNo() + " errorMsg:" + result.getErrorMsg());
				String errorCode = result.getErrorCode();
				if ("0".equals(errorCode)) {// 交易执行成功
					List<Map<String, Object>> resultlist = queryResult.getResultlist();
					if (!resultlist.isEmpty()) {
						map = resultlist.get(0);
					}
				}else{
					throw new RuntimeException("连接区块链异常");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("查询失败");
		}finally {
			ConnectionManager.closeConnection();
		}
		return map;
	}

	@Override
	public String getsaveZsApi(String worksId,String blockId) {
		String jsonObject = JSON.toJSONString("该数据不存在");
		String re = "该数据不存在";
		ImageRenderer render = new ImageRenderer();
		String savePath = "/home/wevolution/admin/uplaod/images";
		Map<String, Object> map = getsampleQueryInfoApi(worksId,blockId);
		String json = JSON.toJSONString(map);
		if (json.equals("{}")) {
			return jsonObject;
		}
		File file = new File(savePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		savePath = savePath + "/" + worksId + ".png";
		File file_img = new File(savePath);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file_img);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String basePath = "http://localhost";
		String url = basePath + "/works/makeZsApi?blockId=" + blockId + "&&worksId=" + worksId;

		try {
			if (render.renderURL(url, out, ImageRenderer.Type.PNG)) {
				String s3Path = "upload/ca/" + worksId + ".png";
				InputStream input = null;
				try {
					input = new FileInputStream(file_img);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				String urlPath = AWSS3Client.uploadToS3(input, s3Path);
				re = urlPath;
				jsonObject = JSON.toJSONString(re);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	@Override
	public ModelAndView getmakeZsApi(ModelAndView mv, String blockId, String worksId) {
		mv.setViewName("userzs");
		Map<String, Object> map = getsampleQueryInfoApi(worksId,blockId);
		String json = JSON.toJSONString(map);
		if (!json.equals("{}")) {
			ResultlistModel model = JSON.parseObject(json, ResultlistModel.class);
			mv.addObject("data", model);
		} else {
			return null;
		}
		return mv;
	}

	@Override
	public Map<String, Object> getQueryWorkOpusInfoApi(String opusId,String blockId,String tableName) {
		Map<String, Object> map = new HashMap<String, Object>();
		BCKeyStoreUtil.createKeyStore(storeName, storePass, alias);
		JBCCClien jbccClient = ConnectionManager.getConnection();
		Map<String, Object> conditionMap = new HashMap<String, Object>();
		conditionMap.put("block_id", blockId);
		conditionMap.put("opus_id", opusId);
		try {
			QueryResult queryResult = jbccClient.selectByContiditon("tdbc",tableName,conditionMap);
			if (queryResult != null) {
				JBCCResult result = queryResult.getJbccResult();
				logger.info("printJbccResult errorCode:" + result.getErrorCode() + " batchNo:" + result.getBatchNo() + " errorMsg:" + result.getErrorMsg());
				String errorCode = result.getErrorCode();
				if ("0".equals(errorCode)) {// 交易执行成功
					List<Map<String, Object>> resultlist = queryResult.getResultlist();
					if (!resultlist.isEmpty()) {
						map = resultlist.get(0);
					}
				}else{
					throw new RuntimeException("连接区块链异常");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("查询失败");
		}finally {
			ConnectionManager.closeConnection();
		}
		return map;
	}

}
