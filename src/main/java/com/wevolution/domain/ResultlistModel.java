package com.wevolution.domain;

/**
 * DESC: 区块链返回的版权信息
 * Created by Hh on 2017/9/19
 */
public class ResultlistModel {
    private String works_nature; //

    private String accomplish_time;

    private String works_name;

    private String pre_hash;

    private String author;

    private String works_id;

    private String tx_hash; //

    private int block_id; //

    private String hash; //

    private String timestamp; //#

    private int tx_lenth;

    private int height;

    private int approved_status;

    public void setWorks_nature(String works_nature) {
        this.works_nature = works_nature;
    }

    public String getWorks_nature() {
        return this.works_nature;
    }

    public void setAccomplish_time(String accomplish_time) {
        this.accomplish_time = accomplish_time;
    }

    public String getAccomplish_time() {
        return this.accomplish_time;
    }

    public void setWorks_name(String works_name) {
        this.works_name = works_name;
    }

    public String getWorks_name() {
        return this.works_name;
    }

    public void setPre_hash(String pre_hash) {
        this.pre_hash = pre_hash;
    }

    public String getPre_hash() {
        return this.pre_hash;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setWorks_id(String works_id) {
        this.works_id = works_id;
    }

    public String getWorks_id() {
        return this.works_id;
    }

    public void setTx_hash(String tx_hash) {
        this.tx_hash = tx_hash;
    }

    public String getTx_hash() {
        return this.tx_hash;
    }

    public void setBlock_id(int block_id) {
        this.block_id = block_id;
    }

    public int getBlock_id() {
        return this.block_id;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return this.hash;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTx_lenth(int tx_lenth) {
        this.tx_lenth = tx_lenth;
    }

    public int getTx_lenth() {
        return this.tx_lenth;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return this.height;
    }

    public int getApproved_status() {
        return approved_status;
    }

    public void setApproved_status(int approved_status) {
        this.approved_status = approved_status;
    }
}
