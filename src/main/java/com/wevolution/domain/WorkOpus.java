package com.wevolution.domain;

import java.io.Serializable;

public class WorkOpus implements Serializable{
    /** 
	* @Fields serialVersionUID :  
	*/ 
	private static final long serialVersionUID = 5597983116442169313L;

    /**
     * 用户id
     */
    protected String userId;

    /**
     * 作品id
     */
    protected String opusId;

    /**
     * 作品名
     */
    protected String opusName;

    /**
     * 作品作者
     */
    protected String author;

    /**
     * 创建时间
     */
    protected String creatTime;

    /**
     * 作品类别
     */
    protected String opusCategory;

    /**
     * 作品类型
     */
    protected String opusType;

    /**
     * 作品地区
     */
    protected String opusArea;

    /**
     * 权利归属
     */
    protected String affiliation;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOpusId() {
        return opusId;
    }

    public void setOpusId(String opusId) {
        this.opusId = opusId;
    }

    public String getOpusName() {
        return opusName;
    }

    public void setOpusName(String opusName) {
        this.opusName = opusName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(String creatTime) {
        this.creatTime = creatTime;
    }

    public String getOpusCategory() {
        return opusCategory;
    }

    public void setOpusCategory(String opusCategory) {
        this.opusCategory = opusCategory;
    }

    public String getOpusType() {
        return opusType;
    }

    public void setOpusType(String opusType) {
        this.opusType = opusType;
    }

    public String getOpusArea() {
        return opusArea;
    }

    public void setOpusArea(String opusArea) {
        this.opusArea = opusArea;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }
}