package com.wevolution.domain;

public class Register {
    public String country;

    public String email;

    public String password;

    public String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    //    public Referral referral;


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public Referral getReferral() {
//        return referral;
//    }
//
//    public void setReferral(Referral Referral) {
//        this.referral = Referral;
//    }
}
