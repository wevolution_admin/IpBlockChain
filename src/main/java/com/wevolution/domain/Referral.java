package com.wevolution.domain;

import java.io.Serializable;

public class Referral implements Serializable {
    public String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
